# **Parsing and alerts for science competitions**

## API for applications

#### 1. Subscribe on our application
    Request: /api/subscribe/app?name={{your_application_name}}
    Response:
        1. succesfull:
            {
                "status": true,
                "message": "OK",
                "token": {{your_token}}        
            }
        2. fail:
            {
                "status": false,
                "message": {{error_message}},
                "token": null        
            }

#### 2. Subscribe your user on notifications
    Request: /api/subscribe/emails?app_name={{your_application_name}}&&token={{your_application_token}}
        1. Post parameters:
            emails - [
                {
                    "email": "user_email",
                    "industries": [
                        {{industry_id}},..
                    ],
                    "email_notify" true or false
                },..
            ]
    Response:
        1. succesfull:
            {
                "status": true,
                "message": "OK"        
            }
        2. fail:
            {
                "status": false,
                "message": {{error_message}}
            }

#### 3. Get competitions and|or notifications for users
    Request: /api/get?app_name={{your_application_name}}&&token={{your_application_token}}
        1. Get parameters:
            competitions    - if this isset, your get competitions by date
            emails          - if this isset, your get notifications by date for your users
            time            - if not exist, last get data date 
    Response:
        1. succesfull:
            {
                "status": true,
                "result": {
                    "competitions": [
                        {
                            "name": {{name}},
                            "grant_size": {{grant_size}},
                            "url": {{url}},
                            "deadline": {{deadline}},
                            "industries": [
                                {{industry_name}},..
                            ]
                        },..
                     ]
                    "emails": {
                        "user_email": [
                            {
                                "id": "competitiom_id",
                                "name": "competitiom_name"
                            },..
                        ],..
                    }                    
                }        
            }
        2. fail:
            {
                "status": false,
                "result": [
                    {{error_message}}
                ]        
            }